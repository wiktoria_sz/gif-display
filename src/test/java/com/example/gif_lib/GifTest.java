package com.example.gif_lib;


import com.example.gif_lib.model.Category;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import com.example.gif_lib.model.Gif;

@SpringBootTest
public class GifTest {


    @Test
    public void testAllCategories(){
        assertEquals(Category.getAllCategories().size(),3);
        assertEquals(Category.getAllCategories().get(1).getName(),"Funny");
        assertEquals(Category.getAllCategories().get(1).getId(),1);
    }

    @Test
    public void testGetGifs(){
        assertEquals(Category.getGifsByCategory(Category.getAllCategories().get(0)).size(),1);
        assertEquals(Category.getGifsByCategory(Category.getAllCategories().get(1)).size(),3);

    }
}
